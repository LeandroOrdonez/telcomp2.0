TelComp 2.0 installation and desployment
=========================================

##Welcome!

Before starting with the installation process you must ensure that option `autoDeploy` in `$CATALINA_HOME/conf/server.xml` is set to true.

##Initial Settings
* Give execution permissions on the setup.sh script. Open a terminal and type:

   ```
   $chmod u+x setup.sh
   ```

##Running
* In the terminal, type ./setup.sh

   ```
   $./setup.sh
   ```
