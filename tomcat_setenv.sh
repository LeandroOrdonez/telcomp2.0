# discourage address map swapping by setting Xms and Xmx to the same value
# http://confluence.atlassian.com/display/DOC/Garbage+Collector+Performance+Issues
export CATALINA_OPTS="$CATALINA_OPTS -Xms2000m"
export CATALINA_OPTS="$CATALINA_OPTS -Xmx6000m"
 
# Increase maximum perm size for web base applications to 4x the default amount
# http://wiki.apache.org/tomcat/FAQ/Memoryhttp://wiki.apache.org/tomcat/FAQ/Memory
export CATALINA_OPTS="$CATALINA_OPTS -XX:PermSize=2000m"
export CATALINA_OPTS="$CATALINA_OPTS -XX:MaxPermSize=6000m"

