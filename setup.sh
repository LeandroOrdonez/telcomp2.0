#!/bin/bash

function timer()
{
    if [[ $# -eq 0 ]]; then
        echo $(date '+%s')
    else
        local  stime=$1
        etime=$(date '+%s')

        if [[ -z "$stime" ]]; then stime=$etime; fi

        dt=$((etime - stime))
        ds=$((dt % 60))
        dm=$(((dt / 60) % 60))
        dh=$((dt / 3600))
        printf '%d:%02d:%02d' $dh $dm $ds
    fi
}

# Starting the installation
t=$(timer)

# Checking if CATALINA_HOME is defined
: ${CATALINA_HOME:?"Need to set CATALINA_HOME non-empty"};

# Setting the tomcat environment
echo "Setting the tomcat environment..."
SET_ENV=$CATALINA_HOME/bin/setenv.sh
PWDIR=$PWD
 # Setting WordNet VM argument on CATALINA_OPTS
COMNT='# Set WordNet path'
SLINE='export CATALINA_OPTS="$CATALINA_OPTS -Dwordnet.database.dir='
echo $COMNT >> $PWDIR/tomcat_setenv_temp.sh
echo $SLINE$PWDIR/WordNet-3.0/dict'"' >> $PWDIR/tomcat_setenv_temp.sh
cat ./tomcat_setenv.sh ./tomcat_setenv_temp.sh >> $SET_ENV
rm ./tomcat_setenv_temp.sh

# Deploying TelComp2.0 Web Apps
echo "Deploying TelComp2.0 Web Apps..."
 # Displaying tomcat console
gnome-terminal -x tail -f $CATALINA_HOME/logs/catalina.out
cp ./telcomp-rep-app.war $CATALINA_HOME/webapps
sleep 15s
cp ./telcomp-rep-web.war $CATALINA_HOME/webapps
sleep 15s
cp ./telcomp-retrieval.war $CATALINA_HOME/webapps
sleep 15s
cp ./TelComp-SCE.war $CATALINA_HOME/webapps
sleep 15s
cp ./TelCompTerminal.war $CATALINA_HOME/webapps
sleep 15s

# Restarting Tomcat
$CATALINA_HOME/bin/catalina.sh stop
sleep 30s
$CATALINA_HOME/bin/catalina.sh start
sleep 40s

echo
echo "MySQL username: "
read MYSQL_USER
echo "MySQL password: "
read -s MYSQL_PASSWD
echo
# Creating telcomp_rep mysql database
#echo "Creating telcomp_rep mysql database..."
#MYSQL=`which mysql`
#Q1="CREATE DATABASE IF NOT EXISTS telcomp_rep;"
#Q2="GRANT ALL ON *.* TO $MYSQL_USER@'localhost' IDENTIFIED BY '$MYSQL_PASSWD';"
#Q3="FLUSH PRIVILEGES;"
#SQL="${Q1}${Q2}${Q3}"
#$MYSQL -uroot -p -e "$SQL"

# Configuring TelComp Apps
echo "Configuring TelComp 2.0 Environment..."
java -jar settings/telcomp-configuration.jar $CATALINA_HOME/webapps/telcomp-rep-app/WEB-INF/classes/telcomp/rep/config/config.properties POS_TAGGER $PWDIR/models/english-left3words-distsim.tagger

java -jar settings/telcomp-configuration.jar $CATALINA_HOME/webapps/telcomp-rep-app/WEB-INF/classes/telcomp/rep/config/config.properties INDEX_LOC $PWDIR/.telcomp_index

java -jar settings/telcomp-configuration.jar $CATALINA_HOME/webapps/telcomp-rep-web/WEB-INF/classes/telcomp/rep/config/config.properties INDEX_LOC $PWDIR/.telcomp_index

java -jar settings/telcomp-configuration.jar $CATALINA_HOME/webapps/telcomp-retrieval/WEB-INF/classes/mwsaf/resources/mwsaf.properties WordNetProperties $CATALINA_HOME/webapps/telcomp-retrieval/WEB-INF/classes/mwsaf/resources/file_properties.xml

java -jar settings/telcomp-configuration.jar --xml $CATALINA_HOME/webapps/telcomp-retrieval/WEB-INF/classes/mwsaf/resources/file_properties.xml dictionary_path $PWDIR/WordNet-3.0/dict

java -jar settings/telcomp-configuration.jar --xml $CATALINA_HOME/webapps/telcomp-retrieval/WEB-INF/classes/mwsaf/resources/wordnet_properties.xml dictionary_path $PWDIR/WordNet-3.0/dict

java -jar settings/telcomp-configuration.jar --persistence $CATALINA_HOME/webapps/telcomp-rep-app/WEB-INF/classes/META-INF/persistence.xml user $MYSQL_USER password $MYSQL_PASSWD

java -jar settings/telcomp-configuration.jar --persistence $CATALINA_HOME/webapps/telcomp-rep-web/WEB-INF/classes/META-INF/persistence.xml user $MYSQL_USER password $MYSQL_PASSWD

java -jar settings/telcomp-configuration.jar --persistence $CATALINA_HOME/webapps/telcomp-retrieval/WEB-INF/classes/META-INF/persistence.xml user $MYSQL_USER password $MYSQL_PASSWD

echo
echo "That's it!"
printf 'Elapsed time: %s\n' $(timer $t)
echo

